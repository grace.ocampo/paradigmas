package datos;

/**
 * @author Graciela
 */

public class Examen {
    private String fecha;
    private String materia;
    private int nota;

    public Examen(String fecha, String materia, int nota) {
        this.fecha = fecha;
        this.materia = materia;
        this.nota = nota;
    }

    public Examen(Examen ex){
        this.fecha = ex.getFecha();
        this.materia= ex.getMateria();
        setNota(ex.getNota());
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "Examen{" +
                "fecha='" + fecha + '\'' +
                ", materia='" + materia + '\'' +
                ", nota=" + nota +
                '}';
    }
}
